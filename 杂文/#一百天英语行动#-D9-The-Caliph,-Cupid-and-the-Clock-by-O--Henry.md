The Caliph, Cupid and the Clock by O. Henry

21 April, 2017*We present the short story "The Caliph, Cupid and the Clock" by O. Henry. The story was originally adapted and recorded by the U.S. Department of State.*

**Prince** Michael of Valleluna sat in the park on the seat he liked best. In the coolness of the night, he felt full of life. The other seats were not filled. Cool weather sends most people home.
The moon was rising over the houses on the east side of the park. Children laughed and played. Music came softly from one of the nearer streets. Around the little park, **cabs** rolled by. The trains that traveled high above the street rushed past. These cabs and trains, with their wild noises, seemed like animals outside the park. But they could not enter. The park was safe and quiet. And above the trees was the great, round, shining face of a lighted clock in a tall old building.
Prince Michael's shoes were old and broken. No shoemaker could ever make them like new again. His clothes were very torn. The hair of his face had been growing for two weeks. It was all colors—gray and brown and red and green-yellow. His hat was older and more torn than his shoes and his other clothes.
Prince Michael sat on the seat he liked best, and he smiled. It was a happy thought to him that he had enough money to buy every house he could see near the park, if he wished. He had as much gold as any rich man in this proud city of New York. He had as many jewels, and houses, and land. He could have sat at table with kings and queens. All the best things in the world could be his—art, pleasure, beautiful women, honor. All the sweeter things in life were waiting for Prince Michael of Valleluna whenever he might choose to take them. But instead he was choosing to sit in torn clothes on a seat in a park.
For he had tasted of the fruit the tree of life. He had not liked the taste. Here, in this park, he felt near to the **beating** heart of the world. He hoped it would help him to forget that taste.
These thoughts moved like a dream through the mind of Prince Michael. There was a smile across his face with its many-colored hair. Sitting like this, in torn clothes, he loved to study other men. He loved to do good things for others. Giving was more pleasant to him than owning all his riches. It was his chief pleasure to help people who were in trouble. He liked to give to people who needed help. He liked to surprise them with princely gifts. But he always gave wisely, after careful thought.
And now, as he looked at the shining face of the great clock, his smile changed. The Prince always thought big thoughts. When he thought of time, he always felt a touch of sadness. Time controlled the world. People had to do what time commanded. Their **comings and goings** were always controlled by a clock. They were always in a hurry, and always afraid, because of time. It made him sad.
After a little while, a young man in evening clothes came and sat upon a seat near the Prince. For half an hour he sat there **nervously**. Then he began to watch the face of the lighted clock above the trees. The Prince could see that the young man had a trouble. He could also see that somehow the clock was part of the trouble.
The Prince rose and went to the young man's seat.
"I am a stranger, and I shouldn't speak to you," he said. "But I can see that you are troubled. I am Prince Michael of Valleluna. I do not want people to know who I am. That is why I wear these torn clothes. It is a small pleasure of mine to help those who need help. First I must feel sure they are worth helping. I think you are. And perhaps your trouble may be ended if you and I together decide what to do about it."
The young man looked up brightly at the Prince. Brightly, but he was still troubled. He laughed, then, but still the look of trouble remained. But he accepted this chance to talk to someone.
"I'm glad to meet you, Prince," he said **pleasantly**. "Yes, I can see you don't want to be known. That's easy to see. Thanks for your offer to help. But I don't see what you can do. It's my own problem. But thanks."
Prince Michael sat down at the young man's side. People often said no to him, but they always said it pleasantly.
"Clocks," said the Prince, "are tied to the feet of all men and women. I have seen you watching that clock. That face commands us to act, whether or not we wish to act. Let me tell you not to trust the numbers on that face. They will destroy you if they can. Stop looking at that clock. What does it know about living men and women?"
"I usually don't look at that clock," said the young man. "I carry a watch, except when I wear evening clothes."
"I know men and women as I know the trees and the flowers," said the Prince, warmly and proudly. "I have studied many years. And I am very rich. There are few troubles that I cannot help. I have read what is in your face. I have found honor and goodness there, and trouble. Please accept my help. I can see that you are wise. Show how wise you are. Do not judge me by my torn clothes. I am sure I can help you."
The young man looked at the clock again, and his face grew darker. Then he looked at a house beside the park. Lights could be seen in many rooms.
"Ten minutes before nine!" said the young man. He raised his hands and then let them fall, as if hope had gone. He stood up and took a quick step or two away.
"Remain!" commanded Prince Michael. His voice was so powerful that the young man turned quickly. He laughed a little.
"I'll wait ten minutes and then I'll go," he said in a low voice, as if only to himself. Then to the Prince he said, "I'll join you. We'll destroy all the clocks. And women, too."
"Sit down," said the Prince softly. "I do not accept that. I do not include women. Women are enemies of clocks. They are born that way. Therefore they are friends of those who wish to destroy clocks. If you can trust me, tell me your story."
The young man sat down again and laughed loudly.
"Prince, I will," he said. He did not believe that Prince Michael was really a prince. His manner of speaking proved that. "Do you see that house, Prince? That house with lights in three windows on the third floor? At six tonight I was in that house with the young lady I am going to—was going to marry. I'd been doing wrong, my dear Prince, and she heard about it. I was sorry. I wanted her to forget it. We are always asking women to forget things like that, aren't we, Prince?
" ‘I want time to think,' she said. ‘I will either forget it forever, or never see your face again. At half-past eight,' she said, ‘watch the middle window on the third floor of this house. If I decide to forget, I will hang out a long white cloth. You will know then that everything is as it was before. And you may come to me. If you see nothing hanging from the window, you will know that everything between us is finished forever.'
"That," said the young man, "is why I have been watching that clock. The time was passed twenty-three minutes ago. Do you see why I am a little troubled, my torn Prince?"
"Let me tell you again," said Prince Michael in his soft voice, "that women are the born enemies of clocks. Clocks are bad, women are good. The white cloth may yet appear."
"Never!" said the young man, **hopelessly**. "You don't know Marian. She is always on time, to the minute. That was the first thing I liked about her. At 8:31, I should have known that everything was finished. I'm going to go West. I'll get on the train tonight. I'll find some way to forget her. Good night—Prince."
Prince Michael smiled his gentle, understanding smile. He caught the other's arm. The bright light in the Prince's eyes was softening. It was dream-like, clouded.
"Wait," he said, "till the clock tells the hour. I have riches and power and I am wiser than most men. But when I hear the clock tell the hour, I am afraid. Stay with me till then. This woman shall be yours. You have the promise of the Prince of Valleluna. On the day you are married I will give you $100,000 and a great house beside the Hudson River. But there must be no clocks in that house. Do you agree to that?"
"Sure," said the young man. "I don't like clocks."
He looked again at the clock above the trees. It was three minutes before nine.
"I think," said Prince Michael, "that I will sleep a little. It has been a long day."
He lay down on the seat, as if he had often done it before.
"You'll find me on this park on any evening when the weather is good," said the Prince. "Come to me when you know the day you'll be married. I'll give you the money."
"Thanks, Prince," said the young man. "That day isn't going to come. But thanks."
Prince Michael fell into a deep sleep. His hat rolled on the ground. The young man lifted it, placed it over the Prince's face, and moved one of the Prince's legs into an easier position. "Poor fellow!" he said. He pulled the torn coat together over the Prince's body.
It was nine. Loud and surprising came the voice of the clock, telling the hour. The young man took a deep breath, and turned for one more look at the house. And he gave a shout of joy.
From the middle window on the third floor, a snow-white wonderful cloth was hanging.
Through the park a man came, hurrying home.
"Will you tell me the time, please?" asked the young man.
The other man took out his watch. "Twenty-nine and a half minutes after eight."
And then he looked up at the clock.
"But that clock is wrong!" the man said. "The first time in ten years! My watch is always—"
But he was talking to no one. He turned and saw the young man running toward the house with three lighted windows on the third floor.
And in the morning two **cops** walked through the park. There was only one person to be seen—a man, asleep on a long park seat. They stopped to look at him.
"It's Michael the Dreamer," said one. "He has been sleeping like this in the park for twenty years. He won't live much longer, I guess."
The other cop looked at something in the sleeper's hand. "Look at this," he said. "Fifty dollars. I wish I could have a dream like that."
And then they gave Prince Michael of Valleluna a hard shake, and brought him out of his dreams and into real life.
*Download activities to help you understand this story [here](http://docs.voanews.eu/en-US-LEARN/2016/05/16/b3175039-5537-4efe-9a37-bd1f521e59e9.pdf).*
*Now it's your turn to use the words in this story. Do you feel that you are often in a hurry? How often do you stop to enjoy the world around you? Let us know in the comments section or on [51VOA.COM](http://www.51voa.com/).*
______________________________________________________________
****Words in This Story****
**prince **– *n.* a male member of a royal family
**cab**(**s**) – *n.* a car that carries passengers to a place for an amount of money that is based on the distance traveled
**beating** – *v.* to make the regular movements needed to pump blood
**comings and goings** – *idm.* the activity of people arriving at and leaving a place
**nervously** – *adv.* done in a way showing feelings of being worried and afraid about what might happen
**pleasantly **- *adv.* done in a way that is friendly and likable
**hopelessly** – *adv.* done in a way that shows no feeling of hope
**cop**(**s**) - *n.* a person whose job is to enforce laws, investigate crimes, and make arrests
