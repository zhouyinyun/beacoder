https://englishthesmartway.com/content/fun-and-effective-way-learn-english-–-speech-shadowing

Fun and effective way to learn English – Speech shadowing
Have you ever wanted to be an **actor**? Regardless of your answer, you will have so much **fun** using this **language learning technique**! :)
The technique is called **Speech shadowing**, or some people refer to it as language parroting.
How does it work? It’s simple. Let me demonstrate the technique in just **3 steps**:
1. Pick some **English audio / video material** you really like
Try to choose materials that have **transcript ****or**** subtitles** – it can be your favourite movie, sitcom or audiobook.
I personally use Speech shadowing technique with [Power English lessons](http://link.ivanottinger.com/power-english-web-05) but you can use any other material you like. Important is to have the text **transcript** of the audio and **enjoy** it.
2. Listen to the chosen material at least once to get to know the content
If you are not familiar with the English material you chose, it's worth to listen to it **at least** once without using Speech shadowing technique. It's easier to apply the technique on materials that you know a bit already.
**Update (August 2015):** The teacher [A.J. Hoge](http://ajhoge.com/) recommends listening to the audio much more - **five to seven times** or more before you start shadowing.
And now the **fun part comes in**. :)
3. Do the shadowing!
Listen to the material again and **try to imitate** actors / narrator. What do I mean by that? Repeat everything what is being said in the audio material. You can try to talk **at**** the same time** as the actor / narrator, **or** you can **wait until he/she finishes** the sentence **and then repeat** what he / she said. It is completely up to you but I personaly prefer talking at the same time as the actor.
**Note:** It's not easy for the first time! :) Yeah, if you are just starting with shadowing technique, don't get discouraged if it feels difficult at first. It's normal, so keep going! After some time **you will get better**. Shadowing will be **easier** for you and **you will improve your English a lot**.
Tips
don’t try to be 100% perfect, **have fun and enjoy your time**
**imitate everything actor does** – not just raw words, but try to imitate his/her **pronunciation**, voice **pitch**, **pauses**, everything you can hear from the actor ;)
if you are looking for movie transcripts, [check out this website](http://www.imsdb.com/) for example
[***check out the Power English program***](http://link.ivanottinger.com/power-english-web-05) - I use these lessons to do the shadowing.
search **youtube** for songs with **onscreen lyrics** and **sing along** with the music video :) (but, keep in mind that some of the lyrics on youtube have mistakes in them)
[***check out the LibriVox free audiobooks catalogue***](https://englishthesmartway.com/content/enjoy-thousands-free-audiobooks) - you can download there **audiobooks** completely for free and use the open project Gutenberg to find necessary transcription of your desired audiobook
do the shadowing with **one audio** for several days - move to a new audio when you feel you had enough
use **headphones** or **earphones**
**exercise while shadowing** - simple walking is enough - it will keep you **energized**, alert and you will **learn more**! :)

Speech shadowing is simple but powerful method to improve your language speaking skills. It **boosts your confidence**, **improves pronunciation** and basically, it’s more of a game where you are having fun and learn language subconsciously.
Instead of giving you any proofs how is this technique fun and effective, try it for yourself with the following video. :)
**What do you think of Speech shadowing? Have you tried this technique? Feel free to share your thoughts in the comment section below**.

If you like this article about speech shadowing, **share it with your English-learning friends** so they can get better in English too! Or simply **click the LIKE button** below to show me that I should write more content like this. **Thank you!** :)
