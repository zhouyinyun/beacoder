今天是第16天，同侪压力以及就像孩子生下来之后它就会自顾自地生长，英语行动也在生长，从美文，到VOA，再到BBC、环球时报新闻，现在，我们来到了一本书。

这本书，就是桥水基金老大写的《原则》，我是从李笑来那里听说这本书。

《原则》这本书，顾名思义，说的是原则。我们说一个人很有原则性，可能是带点贬义，可是，现在在我看来，原则是一个人不断成长的根本。

举个例子：如果持续健身，把运动作为一个生活习惯，那么，十年以后，你的身体在概率上是很有可能比那些不运动的人要好。更具体一点，如果你持续做腹部运动，你的腹肌自然就会出现。所以，原则，说的是未来的事，你按照原则行事，那么，你就活在了未来。

笃信原则或者说道理，是人生的根基。

这本书的中英文在李笑来的知笔墨上是免费公开的，所以，有兴趣的朋友可以直接去他网站上下载，当然，你也可以就这样一点一点的看下去，毕竟，原则需要时间才能真正理解。

Principles are concepts that can be applied over and over again in similar circumstances as distinct from narrow answers to specific questions. Every game has principles that successful players master to achieve winning results. So does life. Principles are ways of successfully dealing with the laws of nature or the laws of life. Those who understand more of them and understand them well know how to interact with the world more effectively than those who know fewer of them or know them less well. Different principles apply to different aspects of life -e.g., there are "skiing principles" for skiing, "parenting principles" for parenting, "management principles" for managing, “investment principles” for investing, etc -and there are over -arching "life principles" that influence our approaches to all things. And, of course, different people subscribe to different principles that they believe work best.

原则是能够在相似场景下反复运用的一套概念，有别于具体问题的狭义回答。每个游戏的获胜者都有自己遵循的原则，生活也是如此。原则是应对自然或生活规律的种种方式。对原则理解越透彻，越能在生活中游刃有余。生活的不同方面也有各自的原则，例如滑雪有“滑雪原则”，做父母的有“父母原则”，管理有“管理原则，投资有“投资原则”等等，也存在支配一切，包罗万象的“生活原则”。当然每个人都有自己认为最有效的原则。 

I am confident that whatever success Bridgewater and I have had has resulted from our operating by certain principles. Creating a great culture, finding the right people, managing them to do great things and solving problems creatively and systematically are challenges faced by all organizations. What differentiates them is how they approach these challenges. The principles laid out in the pages that follow convey our unique ways of doing these things, which are the reasons for our unique results. Bridgewater’s success has resulted from talented people operating by the principles set out here, and it will continue if these or other talented people continue to operate by them. Like getting fit, virtually anyone can do it if they are willing to do what it takes.

我坚信我和桥水联合基金所获得的成功，都是得益于我们遵循了某些原则。创造了良好的企业文化，用对合适的员工，指导他们成就大业，各部门都能以有创意的方式系统解决问题，应对挑战。他们的差异来自应对挑战的方法不同。本书要详述的原则体现了我们独特的做事方法，这也是我们脱颖而出的原因。桥水联合基金的成功得益于一群遵循原则处事的人才们，如果后辈人才继续遵循这些原则，定能续写成功。就像健身一样，只有大家都愿意有所付出，才能有所收获。 
