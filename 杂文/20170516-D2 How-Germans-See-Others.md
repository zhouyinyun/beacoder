How Germans See Others

The Germans generally adore England and have suffered in the past from unrequited love.England used to be the ultimate role model with its amazingly advanced political,social, industrial and technological achievements.The Germans regard the English as being very nice and mostly harmless.

They admire Americans for the (un-German) easygoing pragmatism and dislike them for their (un-German) superficiality.For the Germans,the United States is the headmaster in the school of nations, and accord due respect if not always affection.
Germans are strong believers in authority.If you know how to obey, then you can also be a master runs the refrain.

With the Italian Germans have a close understanding because they have so much history in common. Through wars, invasion and other forms of tourism, a deep and lasting friendship has been established. Italian art treasures, food and beaches are thoroughly appreciated. There is also a connection arising from the fact that Italy and Germany both achieved nationhood in the last century,and are still not entirely sure that this was a good thing.

The French are admired for their sophisticated civilization,and pitied for their inferior culture. The French may have higher spirits, but the Germans have deeper souls.Despite this, Francophilia is widespread among Germans, especially those living close to the French border. 

Like a wistful child looking over the garden fence, Germans envy Mediterranean people for more relaxed attitudes, cultural heritage and warm climate.But only when they are on holiday.
The only people to whom the Germans readily concede unquestioned superiority of Teutonic virtues are the Swiss.No German would argue their supremacy in the fields of order, punctuality,diligence, cleanliness and thoroughness. They have never been to war with the Swiss.If experience has taught them one thing, it is that there is not future outside the community of nations.
No other nation has a stronger sense of the importance of getting along with others. 
Tolerance is not only a virtue, It's a duty.

晨读英语美文，第二天，文章讲述德国人是如何看待其他国家的，我觉得，仅是一家之言，我只接触过一位德国房客，从吴军专栏中有一篇讲到：德国人的智慧——生活是具体的。我想，我们可以在这方面向德国人学习，生活是具体的，魔鬼在细节里，我们也应该用这种思维去改造自己的生活。学习也是具体的，语言也是具体的，一切都在词语中。

