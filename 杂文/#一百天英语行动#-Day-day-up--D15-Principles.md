我找到一本好书，然后有英文和中文，我觉得挺好的，干脆就这么一篇一篇读下去。

这是这本书的第一段。

Principles are concepts that can be applied over and over again in similar circumstances as distinct from narrow answers to specific questions. 

Every game has principles that successful players master to achieve winning results. 

So does life. 

Principles are ways of successfully dealing with the laws of nature or the laws of life. 

Those who understand more of them and understand them well know how to interact with the world more effectively than those who know fewer of them or know them less well. 

Different principles apply to different aspects of life -e.g., there are "skiing principles" for skiing, "parenting principles" for parenting, "management principles" for managing, “investment principles” for investing, etc -and there are over -arching "life principles" that influence our approaches to all things. 

And, of course, different people subscribe to different principles that they believe work best.

哦，我知道，今天是周末，可是，前几天不是偷懒了么。
