Shall I Compare Thee to a summer’s Day

William Shakespeare

Shall I compare thee to a summer’s day?

Thou art more lovely and more temperate.

Rough winds do shake the darling buds of May,

And often is his gold complexion dimmed;

And every fair from fair sometime declines,

By chance , or nature’s changing course, untrimmed:

But thy eternal summer shall not fade,

Nor lose possession of that fair thou owest;

Nor shall death brag thou wanderest in his shade

When in eternal lines to time thou growest.

So long as men can breathe or eyes can see,

So long lives this, and this gives life to thee.

能否把你比作夏日璀璨？

你却比炎夏更可爱温存；

狂风摧残五月花蕊娇妍，

夏天匆匆离去毫不停顿。

苍天明眸有时过于灼热，

金色脸容往往蒙上阴翳；

一切优美形象不免褪色，

偶然摧折或自然地老去。

而你如仲夏繁茂不凋谢，

秀雅风姿将永远翩翩；

死神无法逼你气息奄奄，

你将永生于不朽诗篇。

只要人能呼吸眼不盲，

这诗和你将千秋流芳。
