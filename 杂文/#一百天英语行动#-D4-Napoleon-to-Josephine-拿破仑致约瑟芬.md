D4

Napoleon to Josephine 拿破仑致约瑟芬

I have your letter, my adorable love. It has filled my heart with joy...since I left you I have been sad all the time. My only happiness is near you. I go over endlessly in my thought your kisses, your tears, your delicious jealousy. The charm of my wonderful Josephine kindles a living, blazing fire in my heart and senses. When shall I be able to pass every minute near you, with nothing to do but to love you and nothing to think of but the pleasure of telling you of it and giving you proof of it? I have loved you some time ago; since then I feel that I love you a thousand times better. Ever since I have known you I adore you more every day. That proves how wrong is that saying of La Bruyere "Love comes all of a sudden." 

Ah, let me see some of your faults; be less beautiful, Less graceful, less tender, less good. But never be jealous and never shed tears. Your tears send me out of my mind they set my very blood on fire. Believe me that it is utterly impossible for me to have a single thought that is not yours, a single fancy that is not submissive to your will. Rest well. Restore your health. Come back to me and then at any rate before we die we ought to be able to say:"We were happy for so very many days!" Millions of kisses even to your dog.

　　我收到了你的信，我崇拜的心上人。 你的信使我充满了欢乐……自我与你分手之后，我一直闷闷不乐，愁眉不展。 我唯一的幸福就是伴随着你。 你的吻给了我无限的思索和回味，还有你的泪水和甜蜜的嫉妒。 我迷人的约瑟芬的魅力像一团炽热的火在我的心里燃烧。 什么时候我才能在你身旁度过每分每刻， 除了爱你什么也不做;除了爱你，除了向你倾诉我对你的爱并向你证明我对你的爱的那种愉快，我什么也不想了。 我不敢相信不久前爱过你，自那以后我感到对你的爱更增一千倍。 自与你相识，我一天比一天更崇拜你。 这正好证明了La Bruyere说的“爱，突如其来”多么不切合实际。

 唉，让我来看你的一些美中不足吧。 让你再少几分甜美、再少几分温柔、再少几分妩媚、再少几分姣好吧。 但决不要嫉妒，决不要流泪。 你的眼泪使我神魂颠倒，你的眼泪使我热血沸腾。 相信我，我每分每刻都想着你，绵绵的思念全是因为你。所有的意愿都顺从你。 好好休息，早日康复。 回到我的身边，不管怎么说，在我们谢世之前，我们应当能说：“我们曾有多少个幸福的日子啊!” 我给你千百万次的吻，还吻你的爱犬。
