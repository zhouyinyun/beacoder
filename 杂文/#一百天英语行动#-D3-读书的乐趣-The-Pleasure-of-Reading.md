> 卢照邻《长安古意》：寂寂寥寥扬子居，年年岁岁一床书。唯有南山桂花发，飞来飞去袭人裾。

摘选一段

The Pleasure of Reading 

All the wisdom of the ages, all the stories that have delighted mankind for centuries, are easily and cheaply available to all of us within the covers of books but we must know how to **avail ourselves of **this treasure and how to get the most from it. The most unfortunate people in the world are those who have never discovered how satisfying it is to read good books.

人类世世代代的聪明才智，千百年来令人们陶醉的故事，我们都可以轻而易举地从书籍中获取而又无需耗费许多钱财。不过，我们必须懂得如何利用这笔财富并从中获得最大的收益。世上最不幸的人就是那些从未领略过阅读好书乐趣的人。

美文太长，我们需要最小行动，今天献上一段关于阅读的文字。
