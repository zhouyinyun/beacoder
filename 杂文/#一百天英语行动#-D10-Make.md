Different Meanings of 'Make' in Everyday Speech

By John Russell
25 May, 2017

Imagine you are watching the 1991 American film City Slickers. It tells the story of an unhappy man and two of his friends. The men agree they need a short break from their day-to-day problems. So they decide to go on a trip to the southwestern United States.

In the movie, you hear the following lines:

"When you're a teenager, you think you can do anything – and you do. Your twenties are a blur. Thirties, you raise your family, you make a little money and you think to yourself, 'What happened to my twenties?"

Our report today is not about aging. Instead, it is about a strange verb: make.

Have you ever wondered about the verb make? Have you noticed that it has different meanings in different situations?

On Everyday Grammar, we will explore three meanings of the word make. We will learn how make has different meanings depending on the noun phrase that follows it in a sentence.

The verb make

Make is an irregular verb. Unlike many other verbs, the past tense is not formed by adding an –ed at the end. Instead, the past tense is made.

The literal meaning of make is "to produce or create something." However, this meaning is not common in everyday speech.

everyday grammar

Instead, English speakers often use make to suggest different meanings. They show these meanings by using one of a number of noun phrases after the verb.

Together, these make+noun phrase structures have an idiomatic meaning. In other words, the structure make+ a noun phrase often has a meaning other than what the individual words suggest.

There are many different meanings of the structure make+ a noun phrase.

Two grammar experts, Susan Conrad and Douglas Biber, have identified over one dozen commonly-used make + noun phrase expressions!

Today, we will study three of them. These structures are all polite and can be used in almost any situation – at school, at work, or among friends and family.

#1 Perform an action

One common meaning of make is this: to perform an action.

Make has this meaning when it is followed by a noun phrase such as the bed or a telephone call.

Here are two examples.

You might hear a parent tell a child, "You need to make your bed." Or you might hear someone say, "I would like to make a phone call."

In both examples, the verb make suggests performing an action. So, when an English speaker says "make the bed," he or she means to perform the action of straightening the bedcover and sheets.

When someone says "make a phone call," he or she means the act of going to the phone, calling the number, and speaking to another person.

#2 Plan or decide to do something

A second meaning of the verb make is to plan or decide to do something. Make has this meaning when followed by a noun phrase such as an appointment, plans to, or a decision to.

Imagine you hear this short phone call:

Person 1: Good afternoon, this is Doctor Smith's office.

Person 2: Good afternoon. I would like to make an appointment with Dr. Smith.

Person 1: I'm sorry, he's not available until Friday. Would you like to make an appointment for this Friday?

Person 2: I don't think that will work – I've already made plans to go out of town!

In the exchange, you heard two examples of make+a noun phrase. The words "make an appointment" mean to plan to meet with another person.

When the speaker says, "I've already made plans to go out of town," what she means is that she has already decided to do something – go out of town.

You will hear this structure often in other situations – such as in a news report or on a television show. Almost every day, news agencies report, "The president has made a decision to... " or "The committee has made plans to...."

Now you know that they have the basic meaning of planning or deciding to do something.

#3 Earn money

A third common meaning of make is this: to earn money. Make has this meaning when followed by noun phrases such as a living, money, or a profit.

So, for example, a student might say, "Now that I've finished school, I have to make a living." A businessperson might explain to a financial specialist, "My business made a profit last year."

In both cases, make means the act of earning money.

There was an example of this meaning at the beginning of our report.

"When you're a teenager, you think you can do anything – and you do. Your twenties are a blur. Thirties, you raise your family, you make a little money and you think to yourself, 'What happened to my twenties?"

What can you do?

Now that you have learned about a few meanings of the verb make, you will have to make an attempt to learn a few other possible meanings of it.

The next time you are watching an American film or television broadcast, try to find examples of make+ a noun phrase.

Ask yourself how the speakers are using make, and what nouns or noun phrases go with the verb.

Learning these idiomatic meanings of make is not easy. However, with training and effort, you can make progress.

I'm Alice Bryant.

And I'm John Russell.

John Russell wrote this story for VOA Learning English. George Grow was the editor.

_____________________________________________________________

Words in This Story

blur – n. something that is difficult to remember

irregular – adj. not following the normal patterns by which word forms (such as the past tenses of verbs) are usually created

literal – adj. involving the ordinary or usual meaning of a word

idiomatic – adj. an expression that cannot be understood from the meanings of its separate words but that has a separate meaning of its own

noun phrase – n. a group of words that acts like a noun in a sentence

polite – adj. having or showing good manners or respect for other people

tense – n. a form of a verb suggesting time or length of action

grammar – n. the study of words and their uses in a sentence

dozen – n. a group of 12
