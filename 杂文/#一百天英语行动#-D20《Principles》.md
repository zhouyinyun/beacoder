
![WechatIMG43.jpeg](http://upload-images.jianshu.io/upload_images/743438-4cf598be14c1347a.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

I also believe that those principles that are most valuable to each of us come from our own encounters with reality and our reflections on these encounters –not from being taught and simply accepting someone else’s principles. So, I put these out there for you to reflect on when you are encountering your realities, and not for you to blindly follow. What I hope for most is that you and others will carefully consider them and try operating by them as part of your process for discovering what works best for you. Through this exploration, and with their increased usage, not only will they be understood, but they will evolve from “Ray’s principles” to “our principles,” and Ray will fade out of the picture in much the same way as memories of one’s ski or tennis instructor fade and people only pay attention to what works.So, when digesting each principle, please…

同时，我认为最有价值的原则都不是教出来的或简单接受他人的原则，而是通过有自己的现实经历与反思得出来的。书中列出这些原则，是供你参照反思自己的经历，而不是要你盲目遵循我列的这些原则。我最希望看到你能仔细思忖本书之后内容中详述的各种原则，尝试使用这些原则，摸索出最适合自己的原则。在探寻和不断使用中，不仅理解这些原则，而是有一天，“雷的原则（译者注：作者的名字是雷.达里奥）”悄然逝去，而属于你自己的“我的原则”会跃然纸上。这就像人们学会滑雪或网球后，教练的影子也会逐渐淡去，我们只会记得那些行之有效的东西。 

just what I believe, others will certainly have their own principles, and possibly even their own principles documents, and future managers of Bridgewater will work in their own ways to determine what principles Bridgewater will operate by. At most, this will remain as one reference of principles for people to consider when they are deciding what’s important and how to behave.

本书主要阐述的是我信奉的原则，别人也有他们信奉的原则，他们很可能也有写自己原则的汇总文件。桥水联合基金公司未来的管理者会以他们自己的方式工作，决定公司遵循什么原则。至多，当人们面临重要决定，不知如何入手时，本书能在原则方面提供一些参考。那么在消化每条原则时，请做到以下这条： …ask yourself: “Is it true?”问自己：“这是对的么？”


![WechatIMG1.jpeg](http://upload-images.jianshu.io/upload_images/743438-8b1a2bba0393508f.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
