你肯定已经忘记了自己刚出生花了多久，学会抬头，抬胸，独坐等动作了吧。

这几天，我在练刀工，别人会嫌弃你的动作慢了。

脑海里就浮现了“动作记忆”四个字。

市面上有非常多的一个月学会什么什么？

如果你明白，或者知道一个婴儿出生之后，花了9个月才学会独坐。（本知识来自百度百科）

270天。

一切都是基因、时间中确定好的，正因为有了时间的尺度，我们才是存在的。

所以，静下心来，像一个婴儿一样，练习吧。

这是早晨看到一个山地自行车比赛，运动员骑在一辆普通的山地自行车上，从山顶俯冲，道路险阻，弯弯曲曲，但是他每一个动作都做得很到位，速度极快，看得我立刻清醒不要睡觉了。
![图片来自网络](https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1488599225&di=73fba2340a52a515d26ae7067bb2a929&imgtype=jpg&er=1&src=http%3A%2F%2Fimg3.fengniao.com%2Fforum%2Fattachpics%2F653%2F75%2F26094959.jpg)
但是，对于骑在车上的人，他肯定是一种静心状态，只有注意力全神贯注之后，他才有可能那么从容地面对新路面做出精准的动作,这种静心状体和冥想过后的无我是一样的，都需要无数次的练习，让本来没有的记忆印入大脑，形成新的沟回。

所以，学英语，就是要那么久，学画画，就是要那么久，学什么，都不要想着速成。

PS：冥想第156天，我以为一百天是个关口，其实，时间它本身没有刻度，你只管做，时候到了，自然会有回报。

24号的文留到了25号。

> 从2017年二月二十二日开始，每日写作二百字。这是今天的二百字。二百字，比你发一条微博多六十个字。来吧。写得多写得少写得好写得差，写吧。
