The Different Meanings of 'Take'

By John Russell


Imagine yourself sitting in a movie theater.

You are watching a romantic comedy, a love story with a happy ending, such as the 1989 movie When Harry Met Sally.

In the film, Meg Ryan and Billy Crystal play friends who become lovers. In one part, Harry tells Sally what he loves about her:

"I love that you get cold when it's 71 degrees out. I love that it takes you an hour and a half to order a sandwich."

In this example, you hear Harry use the word take. Have you ever had problems understanding this strange verb?

Today on Everyday Grammar, we will explore the verb take. We will hear how take's meaning changes depending on the noun or noun phrase that follows it in a sentence.

Take

Take is an irregular verb. The past tense of take does not have the usual –ed ending. Instead, the past tense is took.

The literal meaning of take is "to move or carry something from one place to the other." This meaning is rare in everyday speech, note Susan Conrad and Douglas Biber. Both are experts on English grammar.

Instead, native English speakers will often use the word take in a way that has an idiomatic meaning. In other words, the verb take and the words that follow it have a different meaning than the individual words suggest.

One common structure is take + a noun phrase.

Today we will examine three take + noun phrase structures that have idiomatic meanings.

#1 Go in a direction

One common meaning of take is to go in a different direction.

Take has this meaning when followed by noun phrases such as "a right turn" or "a left turn." The word turn is not always added. Sometimes speakers only say "take a right" or "take a left."

Imagine you hear the following exchange:

1: Excuse me, but where is the train station?

2: The train station is about four streets from here. Walk straight and then take a right. After you see the bank, take a left. You'll see the train station.

1: Thanks! So I take a right and then I take a left after the bank?

2: That's right.

1: Great! Have a nice day.

2: You, too!

In this exchange, you heard four examples of the structure take + a noun phrase. In this case, take means to go in a different direction – either the right or the left.

This structure is very useful to know when asking and giving directions!

#2 Make sure that a task is done properly

A second meaning of take is to make sure that an action is done correctly. Take has this meaning when followed by noun phrases such as "care of...", "charge of...", or "responsibility for..."

You might hear this structure in the workplace.

For example, a worker might tell a supervisor, "Don't worry, I'll take care of it."

When someone says this, he or she means that they will carry out the action or complete the work.

When a supervisor plans to be out of the office, he or she might say, "Jane will take charge of the project while I am away."

This statement, about a worker named Jane, means that she is responsible for making sure that work on the project continues while the supervisor is gone.

#3 Spend enough time for a task

A third common meaning of take is to spend time for a task.

Take has this meaning when followed by nouns such as "a minute" or "time."

Sometimes these nouns come in phrases. In other words, adjectives, adverbs, and other words go along with the noun.

Here are a few examples. In school, one student might tell another student, "The homework is easy. It only takes a few minutes."

A teacher might tell a student, "This exam will be difficult. It will probably take a long time."

In both examples, the verb take suggests spending time to complete a task – namely, the homework and the test.

This meaning is similar to the lines you heard at the beginning of this report:

"I love that you get cold when it's 71 degrees out. I love that it takes you an hour and a half to order a sandwich."

In this case, Harry is talking about a set amount of time – an hour and a half. This is the amount of time Sally spends on ordering a sandwich.

What can you do?

Take has many other meanings. The next time you are watching television or reading the news, try to find examples of take + a noun phrase.

Ask yourself what the speakers mean when they use the structure. Do they use it to give one of the meanings we discussed today? Or do they use it to give a different meaning?

It may take a long time, and it may require effort, but you will learn the different meanings of take!

We will leave you with a few words from the American jazz song Take Five. The performer is Al Jarreau.

Just have them take a little time out with me
we'll just take five, just take five

I'm Alice Bryant.

And I'm John Russell.

John Russell wrote this story for Learning English. George Grow was the editor.

We want to hear from you. Write to us in the Comments Section. ________________________________________________________________

Words in This Story

noun phrase – n. a group of two or more words that express a single idea, but do not usually form a complete sentence

irregular – n. grammar not following the normal patterns by which word forms (such as the past tenses of verbs) are usually created

literal – adj. giving the meaning of each individual word

idiomatic – adj. : an expression that cannot be understood from the meanings of its separate words but that has a separate meaning of its own

adverb – n. a word that describes a verb, an adjective, another adverb, or a sentence and that is often used to show time, manner, place, or degree

task – n. a piece of work; an action or duty
